import { API_URL } from '$env/static/private';
import axios from 'axios';

// Create a instance of axios to use the same base url.
const axiosApi = axios.create({
	baseURL: API_URL
});

// function to execute the http get request
export const get = (url: string) => axiosApi.get(url);

// function to execute the http delete request
export const deleteRequest = (url: string) => axiosApi.delete(url);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const post = <D, R = any>(url: string, data: D) => axiosApi.post<R>(url, data);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const put = <D, R = any>(url: string, data: D) => axiosApi.put(url, data);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const patch = <D, R = any>(url: string, data: D) => axiosApi.patch(url, data);
