export type Address = { city: string };

export type Company = object;

export type User = {
	id: number;
	name: string;
	username: string;
	email: string;
	address: Address;
	phone: string;
	website: string;
	company: Company;
};

export type Post = {
	userId: number;
	id: number;
	title: string;
	body: string;
};
