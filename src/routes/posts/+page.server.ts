import { get } from '$lib/server/axios';

export async function load() {
	const posts = await get('/posts');
	return {
		posts: posts.data
	};
}
