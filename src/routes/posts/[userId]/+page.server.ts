import { get } from '$lib/server/axios';

export async function load({ params }) {
	const posts = await get(`/users/${params.userId}/posts`);
	return {
		posts: posts.data
	};
}
