import { get } from '$lib/server/axios';

export async function load() {
	const users = await get('/users');
	return {
		users: users.data
	};
}
